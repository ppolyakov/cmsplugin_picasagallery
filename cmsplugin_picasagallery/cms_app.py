from cms.app_base import CMSApp
from cms.apphook_pool import apphook_pool
from django.utils.translation import ugettext_lazy as _

class PicasaGalleryApp(CMSApp):
    name = _("PicasaGallery App")
    urls = ["cmsplugin_picasagallery.urls"]

apphook_pool.register(PicasaGalleryApp)
  